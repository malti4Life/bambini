package com.bambini.utils;

public class Constant {
    public static final String KEY_DATA = "res_object";
    public static final String KEY_USER_DATA="user_data";

    public static final String KEY_STATUS = "res_code";
    public static final String KEY_MESSAGE = "res_message";

}
