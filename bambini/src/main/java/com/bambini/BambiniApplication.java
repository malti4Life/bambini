package com.bambini;

import android.app.Application;

import com.bambini.webservices.RestClient;

public class BambiniApplication extends Application {
    private static BambiniApplication instance;
    public static BambiniApplication getInstance(){
        return  instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

//        new RestClient();
    }
}
