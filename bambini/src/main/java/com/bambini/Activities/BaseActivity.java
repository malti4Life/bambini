package com.bambini.Activities;

import android.content.Intent;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bambini.R;

public abstract class BaseActivity extends AppCompatActivity {
    protected abstract void initView();

    protected abstract void initToolBar();

    protected abstract int getLayoutResourceId();
    protected TextView titleTextView;

    protected Toolbar toolbar;

    protected ImageView backImageView;

    protected DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResourceId());
        initToolBar();
        initView();
    }
    @Override
    public void onBackPressed() {
        if (drawerLayout != null) {
            if (drawerLayout.isDrawerVisible(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);

            } else {
                backPressed();
            }
        } else {
            backPressed();
        }
    }

    public void backPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        }
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
            overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_right);

        }
    }
    public void navigateToNextActivity(Intent intent, boolean isFinish) {
        startActivity(intent);
        if (isFinish)
            finish();
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
    }



//    public void setActionBarTitle(String title) {
//        if (getSupportActionBar() != null) {
//            getSupportActionBar().setTitle("");
//        }
//
//        titleTextView.setText(Html.fromHtml(title));
//    }



    public void openCloseLeftDrawer() {
        if (drawerLayout.isDrawerVisible(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            drawerLayout.openDrawer(GravityCompat.START);
        }
    }


//    public void showBackButton() {
//        if (toolbar != null) {
//            backImageView = (ImageView) toolbar.findViewById(R.id.row_toolbar_iv_back);
//            backImageView.setVisibility(View.VISIBLE);
//        }
//    }

    public ImageView getBackImageView() {
        return backImageView;
    }

}
